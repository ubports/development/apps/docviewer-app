# Esperanto translation for ubuntu-docviewer-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-docviewer-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-docviewer-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-10 07:09+0000\n"
"PO-Revision-Date: 2024-05-11 23:01+0000\n"
"Last-Translator: phlostically <phlostically@mailinator.com>\n"
"Language-Team: Esperanto <https://hosted.weblate.org/projects/lomiri/lomiri-"
"docviewer-app/eo/>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.5.4\n"
"X-Launchpad-Export-Date: 2017-04-05 07:48+0000\n"

#: lomiri-docviewer-app.desktop.in:7
msgid "/usr/share/lomiri-docviewer-app/docviewer-app.png"
msgstr "/usr/share/lomiri-docviewer-app/docviewer-app.png"

#: lomiri-docviewer-app.desktop.in:8
msgid "Document Viewer"
msgstr "Dokumentmontrilo"

#: lomiri-docviewer-app.desktop.in:9
msgid "documents;viewer;pdf;reader;"
msgstr "dokumentoj;dokumento;vidigilo;pdf;legilo;montrilo;"

#: src/app/qml/common/CommandLineProxy.qml:49
msgid "Some of the provided arguments are not valid."
msgstr "Kelkaj el la havigitaj argumentoj estas nevalidaj."

#: src/app/qml/common/CommandLineProxy.qml:58
msgid "Open lomiri-docviewer-app displaying the selected file"
msgstr "Malfermi lomiri-docviewer-app montrante la elektitan dosieron"

#: src/app/qml/common/CommandLineProxy.qml:65
msgid "Run fullscreen"
msgstr "Plenekrane"

#: src/app/qml/common/CommandLineProxy.qml:71
msgid "Open lomiri-docviewer-app in pick mode. Used for tests only."
msgstr ""
"Malfermi lomiri-dokumento-vidigilo en elekta reĝimo. Uzita nur por testoj."

#: src/app/qml/common/CommandLineProxy.qml:77
msgid ""
"Show documents from the given folder, instead of ~/Documents.\n"
"The path must exist prior to running lomiri-docviewer-app"
msgstr ""
"Montri dokumentojn el la donita dosierujo anstataŭ ~/Dokumentoj.\n"
"La dosiervojo devas ekzisti antaŭ ol ruli lomiri-docviewer-app"

#: src/app/qml/common/DetailsPage.qml:29
#: src/app/qml/loView/LOViewDefaultHeader.qml:93
#: src/app/qml/pdfView/PdfView.qml:317
#: src/app/qml/textView/TextViewDefaultHeader.qml:55
msgid "Details"
msgstr "Detaloj"

#: src/app/qml/common/DetailsPage.qml:42
msgid "File"
msgstr "Dosiero"

#: src/app/qml/common/DetailsPage.qml:47
msgid "Location"
msgstr "Loko"

#: src/app/qml/common/DetailsPage.qml:52
msgid "Size"
msgstr "Grandeco"

#: src/app/qml/common/DetailsPage.qml:57
msgid "Created"
msgstr "Kreita"

#: src/app/qml/common/DetailsPage.qml:62
msgid "Last modified"
msgstr "Laste ŝanĝita"

#: src/app/qml/common/DetailsPage.qml:69
msgid "MIME type"
msgstr "MIME-tipo"

#: src/app/qml/common/ErrorDialog.qml:23
msgid "Error"
msgstr "Eraro"

#: src/app/qml/common/ErrorDialog.qml:26
#: src/app/qml/common/PickImportedDialog.qml:54
#: src/app/qml/common/RejectedImportDialog.qml:38
#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:33
#: src/app/qml/documentPage/SortSettingsDialog.qml:53
msgid "Close"
msgstr "Fermi"

#: src/app/qml/common/PickImportedDialog.qml:29
msgid "Multiple documents imported"
msgstr "Pluraj dokumentoj importitaj"

#: src/app/qml/common/PickImportedDialog.qml:30
msgid "Choose which one to open:"
msgstr "Elektu tiun malfermotan:"

#: src/app/qml/common/RejectedImportDialog.qml:28
msgid "File not supported"
msgid_plural "Files not supported"
msgstr[0] "Dosiero ne eltenita"
msgstr[1] "Dosieroj ne eltenitaj"

#: src/app/qml/common/RejectedImportDialog.qml:29
msgid "Following document has not been imported:"
msgid_plural "Following documents have not been imported:"
msgstr[0] "Jena dokumento ne estis importita:"
msgstr[1] "Jenaj dokumentoj ne estis importitaj:"

#: src/app/qml/common/TextualButtonStyle.qml:48
#: src/app/qml/common/TextualButtonStyle.qml:51
#: src/app/qml/documentPage/DocumentPagePickModeHeader.qml:54
msgid "Pick"
msgstr "Elekti"

#: src/app/qml/common/UnknownTypeDialog.qml:27
msgid "Unknown file type"
msgstr "Nekonata dosierspeco"

#: src/app/qml/common/UnknownTypeDialog.qml:28
msgid ""
"This file is not supported.\n"
"Do you want to open it as a plain text?"
msgstr ""
"Ĉi tiu dosiero ne estas eltenita.\n"
"Ĉu vi volas malfermi ĝin kiel ebenan tekston?"

#: src/app/qml/common/UnknownTypeDialog.qml:38
#: src/app/qml/documentPage/DeleteFileDialog.qml:55
#: src/app/qml/documentPage/DocumentPagePickModeHeader.qml:34
#: src/app/qml/documentPage/DocumentPageSearchHeader.qml:33
#: src/app/qml/loView/LOViewGotoDialog.qml:55
#: src/app/qml/pdfView/DocumentLockedDialog.qml:63
#: src/app/qml/pdfView/OpenLinkDialog.qml:39
#: src/app/qml/pdfView/PdfView.qml:261
#: src/app/qml/pdfView/PdfViewGotoDialog.qml:51
msgid "Cancel"
msgstr "Nuligi"

#: src/app/qml/common/UnknownTypeDialog.qml:44
msgid "Yes"
msgstr "Jes"

#. TRANSLATORS: %1 is the size of a file, expressed in GB
#: src/app/qml/common/utils.js:22
#, qt-format
msgid "%1 GB"
msgstr "%1 GB"

#. TRANSLATORS: %1 is the size of a file, expressed in MB
#: src/app/qml/common/utils.js:26
#, qt-format
msgid "%1 MB"
msgstr "%1 MB"

#. TRANSLATORS: %1 is the size of a file, expressed in kB
#: src/app/qml/common/utils.js:30
#, qt-format
msgid "%1 kB"
msgstr "%1 kB"

#. TRANSLATORS: %1 is the size of a file, expressed in byte
#: src/app/qml/common/utils.js:33
#, qt-format
msgid "%1 byte"
msgstr "%1 bajto"

#: src/app/qml/documentPage/DeleteFileDialog.qml:39
msgid "Delete file"
msgstr "Forigi dosieron"

#: src/app/qml/documentPage/DeleteFileDialog.qml:40
#, qt-format
msgid "Delete %1 file"
msgid_plural "Delete %1 files"
msgstr[0] "Forigi %1 dosieron"
msgstr[1] "Forigi %1 dosierojn"

#: src/app/qml/documentPage/DeleteFileDialog.qml:41
#: src/app/qml/documentPage/DeleteFileDialog.qml:42
msgid "Are you sure you want to permanently delete this file?"
msgid_plural "Are you sure you want to permanently delete these files?"
msgstr[0] "Ĉu vi volas permanente forigi ĉi tiun dosieron?"
msgstr[1] "Ĉu vi volas permanente forigi ĉi tiujn dosierojn?"

#: src/app/qml/documentPage/DeleteFileDialog.qml:61
#: src/app/qml/documentPage/DocumentDelegateActions.qml:25
#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:58
msgid "Delete"
msgstr "Forigi"

#: src/app/qml/documentPage/DocumentDelegateActions.qml:44
msgid "Share"
msgstr "Diskonigi"

#: src/app/qml/documentPage/DocumentEmptyState.qml:27
msgid "No documents found"
msgstr "Neniu dokumento trovita"

#: src/app/qml/documentPage/DocumentEmptyState.qml:28
msgid ""
"Connect your device to any computer and simply drag files to the Documents "
"folder or insert removable media containing documents."
msgstr ""
"Konektu vian aparaton al iu komputilo kaj simple ŝovu dosierojn en la "
"dosierujon 'Dokumentoj' aŭ enmetu demeteblan memorilon enhavantan "
"dokumentojn."

#: src/app/qml/documentPage/DocumentListDelegate.qml:75
msgid "SD card"
msgstr "SD-karto"

#. TRANSLATORS: %1 refers to a time formatted as Locale.ShortFormat (e.g. hh:mm). It depends on system settings.
#. http://qt-project.org/doc/qt-4.8/qlocale.html#FormatType-enum
#: src/app/qml/documentPage/DocumentListDelegate.qml:100
#, qt-format
msgid "Today, %1"
msgstr "Hodiaŭ, %1"

#. TRANSLATORS: %1 refers to a time formatted as Locale.ShortFormat (e.g. hh:mm). It depends on system settings.
#. http://qt-project.org/doc/qt-4.8/qlocale.html#FormatType-enum
#: src/app/qml/documentPage/DocumentListDelegate.qml:105
#, qt-format
msgid "Yesterday, %1"
msgstr "Hieraŭ, %1"

#. TRANSLATORS: this is a datetime formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#: src/app/qml/documentPage/DocumentListDelegate.qml:112
#: src/app/qml/documentPage/DocumentListDelegate.qml:131
msgid "yyyy/MM/dd hh:mm"
msgstr "dd/MM/yyyy hh:mm"

#. TRANSLATORS: this is a datetime formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#: src/app/qml/documentPage/DocumentListDelegate.qml:125
msgid "dddd, hh:mm"
msgstr "dddd, hh:mm"

#: src/app/qml/documentPage/DocumentPageDefaultHeader.qml:27
msgid "Documents"
msgstr "Dokumentoj"

#: src/app/qml/documentPage/DocumentPageDefaultHeader.qml:32
msgid "Search…"
msgstr "Serĉi…"

#: src/app/qml/documentPage/DocumentPageDefaultHeader.qml:39
msgid "Sorting settings…"
msgstr "Agordoj pri ordo…"

#: src/app/qml/documentPage/DocumentPageSearchHeader.qml:56
msgid "search in documents..."
msgstr "serĉi en dokumentoj..."

#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:46
msgid "Select None"
msgstr "Elekti neniun"

#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:46
msgid "Select All"
msgstr "Elekti ĉion"

#: src/app/qml/documentPage/SearchEmptyState.qml:24
msgid "No matching document found"
msgstr "Neniu dokumento koincidas"

#: src/app/qml/documentPage/SearchEmptyState.qml:26
msgid ""
"Please ensure that your query is not misspelled and/or try a different query."
msgstr ""
"Bv. certiĝu pri tio ke via serĉo ne enhavas erarojn kaj/aŭ provu alian "
"serĉon."

#: src/app/qml/documentPage/SectionHeader.qml:30
msgid "Today"
msgstr "Hodiaŭ"

#: src/app/qml/documentPage/SectionHeader.qml:33
msgid "Yesterday"
msgstr "Hieraŭ"

#: src/app/qml/documentPage/SectionHeader.qml:36
msgid "Earlier this week"
msgstr "Plifrue ĉisemajne"

#: src/app/qml/documentPage/SectionHeader.qml:39
msgid "Earlier this month"
msgstr "Plifrue ĉimonate"

#: src/app/qml/documentPage/SectionHeader.qml:41
msgid "Even earlier…"
msgstr "Eĉ pli frue…"

#: src/app/qml/documentPage/SharePage.qml:28
msgid "Share to"
msgstr "Kundividi kun"

#: src/app/qml/documentPage/SortSettingsDialog.qml:26
msgid "Sorting settings"
msgstr "Ordigo-agordoj"

#: src/app/qml/documentPage/SortSettingsDialog.qml:31
msgid "Sort by date (Latest first)"
msgstr "Montri laŭ dato (pli freŝdata unue)"

#: src/app/qml/documentPage/SortSettingsDialog.qml:32
msgid "Sort by name (A-Z)"
msgstr "Montri laŭ nomo (A-Z)"

#: src/app/qml/documentPage/SortSettingsDialog.qml:33
msgid "Sort by size (Smaller first)"
msgstr "Montri laŭ grando (pli malgranda unue)"

#: src/app/qml/documentPage/SortSettingsDialog.qml:47
msgid "Reverse order"
msgstr "Inversigi ordon"

#: src/app/qml/loView/LOViewDefaultHeader.qml:41
#: src/app/qml/textView/TextView.qml:49
msgid "Loading..."
msgstr "Ŝargante…"

#: src/app/qml/loView/LOViewDefaultHeader.qml:45
msgid "LibreOffice text document"
msgstr "Tesktodokumento de LibreOffice"

#: src/app/qml/loView/LOViewDefaultHeader.qml:47
msgid "LibreOffice spread sheet"
msgstr "Kalkultabelo de LibreOffice"

#: src/app/qml/loView/LOViewDefaultHeader.qml:49
msgid "LibreOffice presentation"
msgstr "Prezento de LibreOffice"

#: src/app/qml/loView/LOViewDefaultHeader.qml:51
msgid "LibreOffice Draw document"
msgstr "Dokumento de LibreOffice Draw"

#: src/app/qml/loView/LOViewDefaultHeader.qml:53
msgid "Unknown LibreOffice document"
msgstr "Nekonata dokumento de LibreOffice"

#: src/app/qml/loView/LOViewDefaultHeader.qml:55
msgid "Unknown document type"
msgstr "Nekonata dokumento-speco"

#: src/app/qml/loView/LOViewDefaultHeader.qml:72
msgid "Go to position…"
msgstr "Iri al pozicio…"

#: src/app/qml/loView/LOViewDefaultHeader.qml:86
#: src/app/qml/pdfView/PdfView.qml:310
#: src/app/qml/textView/TextViewDefaultHeader.qml:49
msgid "Disable night mode"
msgstr "Malŝalti nokto-reĝimon"

#: src/app/qml/loView/LOViewDefaultHeader.qml:86
#: src/app/qml/pdfView/PdfView.qml:310
#: src/app/qml/textView/TextViewDefaultHeader.qml:49
msgid "Enable night mode"
msgstr "Ŝalti nokto-reĝimon"

#: src/app/qml/loView/LOViewGotoDialog.qml:30
msgid "Go to position"
msgstr "Iri al pozicio"

#: src/app/qml/loView/LOViewGotoDialog.qml:31
msgid "Choose a position between 1% and 100%"
msgstr "Elektu pozicion inter 1% kaj 100%"

#: src/app/qml/loView/LOViewGotoDialog.qml:62
#: src/app/qml/pdfView/PdfViewGotoDialog.qml:58
msgid "GO!"
msgstr "Iru!"

#: src/app/qml/loView/LOViewPage.qml:173
msgid "LibreOffice binaries not found."
msgstr "Binaraj dosieroj de LibreOffice ne trovitaj."

#: src/app/qml/loView/LOViewPage.qml:176
msgid "Error while loading LibreOffice."
msgstr "Eraro dum la ŝarĝado de LibreOffice."

#: src/app/qml/loView/LOViewPage.qml:179
msgid ""
"Document not loaded.\n"
"The requested document may be corrupt or protected by a password."
msgstr ""
"Dokumento ne ŝarĝita.\n"
"La petita dokumento eble estas difektita aŭ protektita de pasvorto."

#: src/app/qml/loView/LOViewPage.qml:231
msgid "This sheet has no content."
msgstr "Ĉi tiu folio enhavas nenion."

#. TRANSLATORS: 'LibreOfficeKit' is the name of the library used by
#. Document Viewer for rendering LibreOffice/MS-Office documents.
#. Ref. https://docs.libreoffice.org/libreofficekit.html
#: src/app/qml/loView/Splashscreen.qml:45
msgid "Powered by LibreOfficeKit"
msgstr "Funkciigita de LibreOfficeKit"

#. TRANSLATORS: Please don't add any space between "Sheet" and "%1".
#. This is the default name for a sheet in LibreOffice.
#: src/app/qml/loView/SpreadsheetSelector.qml:64
#, qt-format
msgid "Sheet%1"
msgstr "Folio%1"

#: src/app/qml/loView/ZoomSelector.qml:122
#: src/app/qml/pdfView/ZoomSelector.qml:119
msgid "Fit width"
msgstr "Adapti larĝecon"

#: src/app/qml/loView/ZoomSelector.qml:123
msgid "Fit height"
msgstr "Adapti altecon"

#: src/app/qml/loView/ZoomSelector.qml:124
#: src/app/qml/pdfView/ZoomSelector.qml:121
msgid "Automatic"
msgstr "Aŭtomate"

#: src/app/qml/lomiri-docviewer-app.qml:134
msgid "File does not exist."
msgstr "Dosiero ne ekzistas."

#: src/app/qml/pdfView/DocumentLockedDialog.qml:25
msgid "Document is locked"
msgstr "Dokumento estas ŝlosita"

#: src/app/qml/pdfView/DocumentLockedDialog.qml:26
msgid "Please insert a password in order to unlock this document"
msgstr ""

#: src/app/qml/pdfView/DocumentLockedDialog.qml:50
msgid "Entered password is not valid"
msgstr ""

#: src/app/qml/pdfView/DocumentLockedDialog.qml:69
msgid "Unlock"
msgstr "Malŝlosi"

#: src/app/qml/pdfView/LinkHint.qml:36
#, qt-format
msgid "Open link externally: %1"
msgstr ""

#: src/app/qml/pdfView/LinkHint.qml:37
#: src/app/qml/pdfView/OpenLinkDialog.qml:28
#, qt-format
msgid "Go to page %1"
msgstr "Iri al paĝo %1"

#: src/app/qml/pdfView/OpenLinkDialog.qml:28
msgid "Open link externally"
msgstr "Malfermi hiperligon ekstere"

#: src/app/qml/pdfView/OpenLinkDialog.qml:29
msgid "Are you sure?"
msgstr "Ĉu vi certas?"

#: src/app/qml/pdfView/OpenLinkDialog.qml:45
msgid "Open"
msgstr "Malfermi"

#: src/app/qml/pdfView/OpenLinkDialog.qml:45
msgid "Go to destination"
msgstr "Iri al celo"

#. TRANSLATORS: "Contents" refers to the "Table of Contents" of a PDF document.
#: src/app/qml/pdfView/PdfContentsPage.qml:31
#: src/app/qml/pdfView/PdfView.qml:230
msgid "Contents"
msgstr "Enhavo"

#. TRANSLATORS: the first argument (%1) refers to the page currently shown on the screen,
#. while the second one (%2) refers to the total pages count.
#: src/app/qml/pdfView/PdfPresentation.qml:51
#: src/app/qml/pdfView/PdfView.qml:49
#, qt-format
msgid "Page %1 of %2"
msgstr "Paĝo %1 el %2"

#: src/app/qml/pdfView/PdfView.qml:284
msgid "Search"
msgstr "Serĉi"

#: src/app/qml/pdfView/PdfView.qml:294
msgid "Go to page..."
msgstr "Iri al la paĝo..."

#: src/app/qml/pdfView/PdfView.qml:303
msgid "Presentation"
msgstr "Prezento"

#: src/app/qml/pdfView/PdfView.qml:324
msgid "Rotate 90° right"
msgstr "Turni 90° dekstren"

#: src/app/qml/pdfView/PdfView.qml:341
msgid "Rotate 90° left"
msgstr "Turni 90° maldekstren"

#: src/app/qml/pdfView/PdfViewGotoDialog.qml:26
msgid "Go to page"
msgstr "Iri al la paĝo"

#: src/app/qml/pdfView/PdfViewGotoDialog.qml:27
#, qt-format
msgid "Choose a page between 1 and %1"
msgstr "Elektu paĝon inter 1 kaj %1"

#: src/app/qml/pdfView/ZoomSelector.qml:120
msgid "Fit page"
msgstr "Adapti al la paĝo"

#. TRANSLATORS: This string is used for renaming a copied file,
#. when a file with the same name already exists in user's
#. Documents folder.
#.
#. e.g. "Manual_Aquaris_E4.5_ubuntu_EN.pdf" will become
#. "Manual_Aquaris_E4.5_ubuntu_EN (copy 2).pdf"
#.
#. where "2" is given by the argument "%1"
#.
#: src/plugin/file-qml-plugin/docviewerutils.cpp:119
#, qt-format
msgid "copy %1"
msgstr "%1-a kopio"

#~ msgid "Search..."
#~ msgstr "Serĉi..."

#~ msgid "Sorting settings..."
#~ msgstr "Ordigo-agordoj..."

#~ msgid "Switch to single column list"
#~ msgstr "Ŝanĝi al unukolona listo"

#~ msgid "Switch to grid"
#~ msgstr "Ŝanĝi al krado"

#~ msgid "Back"
#~ msgstr "Reen"

#~ msgid "Go to position..."
#~ msgstr "Iri al pozicio..."

#~ msgid "File does not exist"
#~ msgstr "La dosiero ne ekzistas"
